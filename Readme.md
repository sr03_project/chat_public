# The Chat

### INSIDE THIS REPOS
This repo contains a java webserver and an api for **THE CHAT** app. The webserver is dedicated for admin interface and the api is designed for normal users. 

### Repo Structure
This repo follow a standard structure for a Spring Java project. We have added : 
- Requests package containing our own http requests test.
- Scripts folder containing a script for creating table in our database.
- Controller package contains controller for both webserver and API.
- Service for database interation.
- websocket for streamlining communication with clients.
- Model for data handling.
- Email.

### How To use
To use the app, follow this different steps:
- Create a postgres database with thea dedicated user and correct rights. You can use : 
```shell
psql -U postgres
``` 
then 
```sql
CREATE DATABASE "chatSR03";
CREATE USER chat WITH PASSWORD 'chatPASSWORD';
GRANT ALL PRIVILEGES ON DATABASE "chatSR03" TO chat;
```
- You create our database using script/create.sql script:
```shell
psql -U chat -d chatSR03 -f /path/to/create.sql
``` 
- Run the project using your IDE or Maven with `mvn spring-boot:run`. Admin webserver is listenning to 8080, default user is `admin` and `pass` as password. Ensure you change it first.

Enjoy :-)


### Architecture
The chat works following this scheme : 
![Alt text](architecture.png)

The chat application distinguishes between two main types of users: simple users and admin users. The architecture is designed to handle their interactions efficiently through a combination of React, WebSocket, and Spring components.

#### Simple Users:

- Accessing the Website:
    - Simple users visit our website using a specific URL. 
    - Upon accessing the URL, our React server responds by rendering a single-page application (SPA).
- Navigating the Website:
    - As users navigate through the website, their actions generate requests to our API endpoints. These endpoints are served by our Spring server.
- Opening a Chat:
    - When a user opens a chat, a full-duplex connection is established with our WebSocket server. This allows for real-time messaging capabilities.

#### Admin Users:

- Accessing the Website:
    - Admin users visit our website using a separate URL dedicated to administrative tasks.
    - Upon accessing the URL, our Spring server renders a custom webpage using Thymeleaf templates and responds to the admin client.

### Communication and Data Flow
- WebSocket Server:
    - Manages real-time messaging between users.
    - Communicates with the Service layer to interact with the database.

- API Endpoints:
    - Provide RESTful services for the application.
    - Handle requests from the React front-end and return appropriate responses.
    - Communicate with the Service layer for database operations.

- Web Server:
    - Serves the initial HTML, CSS, and JavaScript files for the React application.
    - Also renders Thymeleaf templates for admin users.

- Service Layer:
    - Acts as an intermediary between the API endpoints, WebSocket server, and the database.
    - Contains the business logic of the application.
    - Communicates with PostgreSQL for data persistence.

- Database (PostgreSQL):
    - Stores all the application data including user information, chat messages, and administrative data.
    - Accessible by the Service layer for CRUD operations.

### Class Diagram and Model
Our database is structured as follow :
![Alt text](MCD.png)

#### Specifications of our database.
The membership table is the result of an n-to-n association between users and channels. It is used to manage which users are members of which channels.

The invitations table has three foreign keys to establish relationships with the users and channels tables. The primary key is an auto-generated identifier, making it easier to handle invitation records.


### Eco Index score

![Alt text](eco-index.png)

Performance, accessibility, and best practices criteria are well met, and the score is quite satisfactory. 
The SEO score, which pertains to the efforts put into the development of the search engine optimization aspect, is not good but is not a priority for this project.

### Security Analysis

- Sql injection

SQL injection is a security vulnerability that allows an attacker to interfere with the queries an application makes to its database, potentially manipulating or accessing unauthorized data.

```
@SuppressWarnings("unchecked")
    public List<User> searchPastUsers(String query) {
        Query q = em.createQuery("SELECT u FROM User u WHERE (u.firstName LIKE :query OR u.lastName LIKE :query OR u.email LIKE :query) AND u.activated = false ORDER BY u.id");
        q.setParameter("query", "%" + query + "%");
        return q.getResultList();
    }
```

For example, we use this kind of query that protects the website against SQL injection by using parameterized queries, which ensure that user input is treated as a parameter rather than executable code. 
The `setParameter` method binds the user input (`query`) to the query in a safe manner. 
As a result, any potentially malicious input is escaped and cannot alter the SQL structure.

- CORS attacks

Cross-Origin Resource Sharing (CORS) attacks, occur when a malicious site attempts to access restricted resources on a different domain through a user's browser.

We can then use the following function in our code in order to protect the website against this kind of attack :
```
setAllowedOrigins("http://localhost:3000/*");
```

- XSS attack

XSS attacks involve injecting malicious scripts into web pages viewed by other users, exploiting vulnerabilities in web applications to execute arbitrary code in the context of another user's session.

We can then use the following function before forwarding the messages to other users :
```
String escapedMsgToSend = StringEscapeUtils.escapeHtml4(msgToSend);
```

This ensures that any HTML or JavaScript code in the message is rendered as plain text, thus preventing the execution of potentially harmful scripts.