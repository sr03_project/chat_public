package fr.utc.sr03.services;


import fr.utc.sr03.model.User;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;
import org.springframework.stereotype.Repository;
import jakarta.transaction.Transactional;

import java.util.List;
import java.util.logging.Logger;

@Repository
@Transactional
public class UserService {
    @PersistenceContext
    EntityManager em;
    private static final Logger logger = Logger.getLogger(UserService.class.getName());
    //CRUD

    public void addUser(User user) throws Exception {
        // verify if the email is unique
        if( getUserByEmail(user.getEmail()) != null){
            logger.warning("User "+ user.getLastName() + " " + user.getLastName() + " | " + user.getEmail() + " is already in the database");
            throw new Exception("Email already used");
        }
        em.persist(user);
    }

    public User getUser(int id){
        //return user
        return em.find(User.class, id);
    }

    public void updateUser(User user){
        em.merge(user);
    }

    public void deleteUser(int id){
        //delete using primary key
        em.remove(getUser(id));
    }

    //Other operations

    @SuppressWarnings("unchecked")
    public List<User> getUsers(){
        Query q = em.createQuery("SELECT u FROM User u WHERE u.activated = true ORDER BY u.id");
        return q.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<User> getPastUsers(){
        Query q = em.createQuery("SELECT u FROM User u WHERE u.activated = false ORDER BY u.id");
        return q.getResultList();
    }

    public User getUserByEmail(String email){
        Query q = em.createQuery("SELECT u FROM User u WHERE u.email = :email");
        q.setParameter("email", email);
        List<User> resultList = q.getResultList();
        if (!resultList.isEmpty()) {
            return resultList.get(0); // Return the first user found, normally the element is unique.
        } else {
            return null; // No user found with the given email
        }
    }

    @SuppressWarnings("unchecked")
    public List<User> searchUsers(String query) {
        Query q = em.createQuery("SELECT u FROM User u WHERE (u.firstName LIKE :query OR u.lastName LIKE :query OR u.email LIKE :query) AND u.activated = true ORDER BY u.id");
        q.setParameter("query", "%" + query + "%");
        return q.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<User> searchPastUsers(String query) {
        Query q = em.createQuery("SELECT u FROM User u WHERE (u.firstName LIKE :query OR u.lastName LIKE :query OR u.email LIKE :query) AND u.activated = false ORDER BY u.id");
        q.setParameter("query", "%" + query + "%");
        return q.getResultList();
    }


}
