package fr.utc.sr03.controller;

import fr.utc.sr03.model.Channel;
import fr.utc.sr03.model.Invitation;
import fr.utc.sr03.model.User;
import fr.utc.sr03.services.ChannelService;
import fr.utc.sr03.services.UserService;
import jakarta.annotation.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;


/**
 * Controller for API request related to Invitation
 *
 * */

@CrossOrigin(origins = "*")
@RestController
public class ApiInvitationController {
    @Resource
    private UserService userService;

    @Resource
    private ChannelService channelService;

    private static final Logger logger = Logger.getLogger(ApiInvitationController.class.getName());

    @GetMapping(value = "/api/invitations/listForUser/{userId}")
    public ResponseEntity<List<Invitation>> getInvitationForUser(@PathVariable(name = "userId") int userID) {
        User u = userService.getUser(userID);
        if (u == null) {
            logger.warning("User with ID " + userID + " not found");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        List<Invitation> inv = channelService.getInvitationsForUserInvited(u);

        return ResponseEntity.status(HttpStatus.OK).body(inv);
    }

    @PatchMapping(value = "/api/invitations/accept/{userId}")
    public HttpStatus acceptInvitation(@PathVariable (name = "userId") int userID, @RequestBody Invitation invitation) {
        User user = userService.getUser(userID);

        if ( user == null || !user.equals(invitation.getInvited())){
            // ensure that only the invited user can accept the invitation
            return HttpStatus.UNAUTHORIZED;
        }

        // remove the invitation from the database
        channelService.deleteInvitation(invitation.getId());
        // add the user to the channel
        Channel c = channelService.getChannel(invitation.getChannel().getId());
        c.addMember(user);
        channelService.updateChannel(c);
        logger.info(" User " + user.getId() + " join " + invitation.getChannel().getId());
        return HttpStatus.OK;
    }

    @PatchMapping(value = "/api/invitations/refuse/{userId}")
    public HttpStatus refuseInvitation(@PathVariable (name= "userId")int userId, @RequestBody Invitation invitation) {
        User user = userService.getUser(userId);
        if ( user == null || !user.equals(invitation.getInvited())){
            return HttpStatus.UNAUTHORIZED;
        }
        channelService.deleteInvitation(invitation.getId());
        logger.info(" User " + user.getId() + " refuse to " + invitation.getChannel().getId());
        return HttpStatus.OK;
    }


    // This function creates an invitation for an user to a channel.
    @PostMapping(value= "/api/invitations/invite/")
    public ResponseEntity<String> inviteUser(@RequestParam(name = "inviterID") int inviterID,
                                             @RequestParam(name = "invitedEmail") String invitedEmail,
                                             @RequestParam(name = "channelID") int channelID) {
        User inviter = userService.getUser(inviterID);
        User invited = userService.getUserByEmail(invitedEmail);
        Channel channel = channelService.getChannel(channelID);

        // we verify that every data requested exists
        if (inviter == null || invited == null || channel == null) {
            logger.warning("User with ID " + inviterID + " tried to invite user " + invitedEmail + " to channel " + channelID );
            return new ResponseEntity<>("BAD_REQUEST", HttpStatus.BAD_REQUEST);
        }

        // only a member of a channel can invite other members. and we can only invite a non-member to a channel
        if (!channel.isMember(inviter) || channel.isMember(invited)) {
            logger.warning(" User : " + inviterID + " is not a member of channel : " + channelID + " he can't invite user " + invitedEmail);
            return new ResponseEntity<>("UNAUTHORIZED", HttpStatus.UNAUTHORIZED);
        }

        // create the invitation
        Invitation invitation = new Invitation(inviter, invited, channel);

        // Ensure that this invitation for this channel is not already in the database.
        List<Invitation> invitations = channelService.getInvitationsForUserInvited(invited);
        for (Invitation i : invitations) {
            if (i.equals(invitation)) {
                // the invitation already exists, send CONFLICT.
                return new ResponseEntity<>("CONFLICT", HttpStatus.CONFLICT);
            }
        }
        channelService.addInvitation(invitation);

        return new ResponseEntity<>("OK", HttpStatus.OK);
    }

}
