package fr.utc.sr03.controller;


import at.favre.lib.crypto.bcrypt.BCrypt;
import fr.utc.sr03.model.User;
import fr.utc.sr03.services.UserService;
import jakarta.annotation.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Controller for API request related to User
 *
 * */

@CrossOrigin(origins = "*") // Allow requests from all origin
@RestController
public class ApiUserController {

    @Resource
    private UserService userService;

    private static final Logger logger = Logger.getLogger(ApiUserController.class.getName());

    @PostMapping(value = "/api/users/create")
    public void create(@RequestBody User entity){
        try {
            userService.addUser(entity);
        } catch (Exception e) {
            logger.warning("Attempt to create a user with an existing email Failed: " + entity.getEmail());
        }
    }

    // Not every users can list users.
    /* @GetMapping(value = "/api/users/list")
    public List<User> getUsers(){
        return userService.getUsers();
    } */

    @PostMapping(value = "/api/users/authenticate")
    public  ResponseEntity<User> authenticate(@RequestBody Map<String, String> request){
        // ResponseEntity<User> is used for sending http response with body.
        try {
            String email = request.get("email");
            String password = request.get("password");

            User u = userService.getUserByEmail(email);
            if(u == null){
                logger.warning("Authentication failed, wrong email address");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
            }

            // password check
            BCrypt.Result result = BCrypt.verifyer().verify(password.toCharArray(), u.getPassword());
            if (result.verified) {
                return ResponseEntity.ok(u);
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
            }
        } catch (Exception e) {
            logger.warning(" Error occurred when authenticate : "+ request.get("email") + " -> " + e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST ).body(null);
        }
    }

    @PostMapping(value="/api/users/updateUser")
    public ResponseEntity<User> updateUser(@RequestParam("id") int id,
                                        @RequestParam("firstName") String firstName,
                                        @RequestParam("lastName") String lastName,
                                        @RequestParam("email") String email,
                                        @RequestParam("password") String password,
                                        @RequestParam("admin") boolean admin,
                                        @RequestParam("newPassword") String newPassword) {
        User userToUpdate = userService.getUser(id);

        if(userToUpdate == null){
            logger.warning("Can not update user with id " + id);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }

        // check if the old password is equal to previous.
        BCrypt.Result result = BCrypt.verifyer().verify(password.toCharArray(), userToUpdate.getPassword());
        if (!result.verified) {
            logger.warning("User with id " + id + " try to update data with wrong password");
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
        }

        userToUpdate.setFirstName(firstName);
        userToUpdate.setLastName(lastName);
        userToUpdate.setEmail(email);

        if (!newPassword.isEmpty()){
            userToUpdate.setPassword(newPassword); // in case a new password is provided we update it
        }
        // This endpoint doesn't allow user to change their role in the app.

        userService.updateUser(userToUpdate);
        return ResponseEntity.ok(userToUpdate);
    }

}
