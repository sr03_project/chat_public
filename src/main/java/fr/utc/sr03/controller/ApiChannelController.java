package fr.utc.sr03.controller;

import fr.utc.sr03.model.Channel;
import fr.utc.sr03.model.Invitation;
import fr.utc.sr03.model.User;
import fr.utc.sr03.services.ChannelService;
import fr.utc.sr03.services.UserService;
import jakarta.annotation.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Controller for API request related to Channel and Membership
 *
 * */

@CrossOrigin(origins = "*")
@RestController
public class ApiChannelController {
    @Resource
    private ChannelService channelService;
    @Resource
    private UserService userService;

    private static final Logger logger = Logger.getLogger(ApiChannelController.class.getName());

    /**
     *  Channels
     *
     * */
    @CrossOrigin(origins = "*")
    @PostMapping(value="/api/channels/createChannel/{userId}")
    public HttpStatus createChannel(@RequestBody Channel entity,
                              @PathVariable(name = "userId") int userId){
        try {
            //Retrieve the user from the DB
            User creator = userService.getUser(userId);
            //Create the channel setting the creator
            Channel c = new Channel(entity.getTitle(), entity.getDate(), entity.getDuration(), entity.getDescription());
            c.setCreator(creator);
            c.addMember(creator);
            //Persist the channel
            channelService.addChannel(c);
            return HttpStatus.OK;
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage());
            return HttpStatus.BAD_REQUEST;
        }
    }

    @GetMapping(value="api/channels/listChannels")
    public ResponseEntity<List<Channel>> getChannels(){
        List<Channel> channels = channelService.getChannels();
        List<Channel> result = new ArrayList<>(channels);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }


    /**
     *  Membership
     *
     * */
    @PostMapping(value = "api/channels/addMember")
    public String addMember(@RequestParam(name = "userId") int userId, @RequestParam(name = "channelId") int channelId){
        //Retrieve the channel
        Channel channel = channelService.getChannel(channelId);
        User user = userService.getUser(userId);
        channel.addMember(user);
        channelService.updateChannel(channel);
        return "Member added !";
    }

    @PostMapping(value = "api/channels/inviteUser")
    public String createInvitation(@RequestParam(name = "inviter") int inviter_id, @RequestParam(name = "invited") int invited_id, @RequestParam(name = "channel") int channel_id){
        // Here we retrieve the information from the request parameter
        User inviter = userService.getUser(inviter_id);
        User invited = userService.getUser(invited_id);
        Channel channel = channelService.getChannel(channel_id);
        Invitation invitation = new Invitation(inviter, invited, channel);
        channelService.addInvitation(invitation);
        return "Invitation created !";
    }


    // Function not used !!!
    /* @PostMapping(value = "api/channels/createUserForChannel/{channelId}")
    public String createUserForChannel(@RequestBody User entity,
                                       @PathVariable(name = "channelId") int channelId) {
        // System.out.println("\nCreate a new User and assign to an existing Channel." + "\n");

        User user = new User(entity.getFirstName(), entity.getLastName(), entity.getEmail(),
                entity.getPassword(), entity.isAdmin());

        try {
            userService.addUser(user);
        } catch (Exception e) {
            logger.warning("Attempt to create a user with an existing email Failed: " + entity.getEmail());
        }
        System.out.println("\nSaved user :: " + user + "\n");

        Channel channel = channelService.getChannel(channelId);
        System.out.println("\nChannel details :: " + channel.toString() + "\n");

        channel.getMembers().add(user);
        channelService.updateChannel(channel);

        System.out.println("\nUser assigned to the Channel." + "\n");

        return "User saved and assigned to the Channel!!!";
    } */

    @GetMapping(value = "api/channels/listChannels/{userId}")
    public ResponseEntity<List<Channel>> getChannelsForUser(@PathVariable(name = "userId") int userID){
        User user = userService.getUser(userID);
        if (user == null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }

        List<Channel> allChannels = channelService.getChannels() ; // Get every channel
        List<Channel> result = new ArrayList<>();
        for( Channel c : allChannels){ // filter channel where the requested user is member.
            if( c.isMember(user)){
                result.add(c);
            }
        }
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    // This  function list every channels in the database and return the channels where the user is admin of.
    @GetMapping(value = "api/channels/listChannelsOfCreator/{userId}")
    public ResponseEntity<List<Channel>> getChannelsForCreator(@PathVariable(name = "userId") int userID){
        User user = userService.getUser(userID);
        if (user == null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }

        List<Channel> allChannels = channelService.getChannels() ;
        List<Channel> result = new ArrayList<>();
        for( Channel c : allChannels){
            if( c.isCreator(user)){
                result.add(c);
            }
        }
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }


    @PostMapping(value = "api/channels/removeChannel/{channelId}")
    public String removeChannel(@PathVariable(name = "channelId") int channelId){
        Channel channel = channelService.getChannel(channelId);
        if (channel == null){
            return "Channel not found!";
        }

        //Removal of all the relative invitations
        List<Invitation> invitations = channelService.getInvitations();
        for (Invitation invitation : invitations){
            if (invitation.getChannel().getId() == channelId){
                channelService.deleteInvitation(invitation.getId());
            }
        }

        //Removal of the channel
        channelService.deleteChannel(channel.getId());
        return "Channel removed!";
    }


    @PostMapping(value = "api/channels/leaveChannel/{userId}/{channelId}")
    public String leaveChannel(@PathVariable(name = "userId") int userId, @PathVariable(name = "channelId") int channelId){
        Channel channel = channelService.getChannel(channelId);
        if (channel == null){
            return "Channel not found!";
        }
        User user = userService.getUser(userId);
        if (user == null){
            return "User not found!";
        }
        if (!channel.isMember(user)){
            return "User not in the chat";
        }

        //If it is the only user => remove the user then remove the channel
        if (channel.getMembers().size()==1){
            channel.removeMember(user);
            channelService.deleteChannel(channel.getId());
            return "User left and channel deleted";
        } else { //If creator, set another one before removing the user
            if (channel.isCreator(user)) {
                channel.setCreator(channel.getRandomMember());
            }
            channel.removeMember(user);
            channelService.updateChannel(channel);
            return "User left the channel";
        }
    }




    @PatchMapping(value = "api/channels/updateChannel/{userId}")
    public HttpStatus updateChannel(@PathVariable(name = "userId") int userId, @RequestBody Channel entity){
        User user = userService.getUser(userId);
        Channel channel = channelService.getChannel(entity.getId()); // Get the complete channel corresponding to data.

        if ( user == null || channel == null || !channel.isCreator(user)){
            // ensure that only the creator can update channel information.
            logger.warning("user trying to update channel without right : user => " + user.getId() + " user.email => " + user.getEmail() + " ; requestedChannel => " +  entity.getTitle() );
            return HttpStatus.UNAUTHORIZED;
        }

        // update the value of channel with the new value
        channel.setTitle(entity.getTitle());
        channel.setDate(entity.getDate());
        channel.setDuration(entity.getDuration());
        channel.setDuration(entity.getDuration());
        channel.setDescription(entity.getDescription());

        channelService.updateChannel(channel);
        return HttpStatus.OK;
    }

    @GetMapping(value = "/api/channels/listMembers/{channelId}")
    public ResponseEntity<Set<User>>  listMembersForChannel(@PathVariable(name = "channelId") int chanID) {
        Channel channel = channelService.getChannel(chanID) ;
        if(channel == null) {
            logger.warning("Trying to list users from a wrong channel !");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        return ResponseEntity.status(HttpStatus.OK).body(channel.getMembers());
    }
}
