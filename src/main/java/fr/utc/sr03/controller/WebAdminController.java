package fr.utc.sr03.controller;

import fr.utc.sr03.model.Channel;
import fr.utc.sr03.model.Invitation;
import fr.utc.sr03.model.User;
import fr.utc.sr03.services.ChannelService;
import fr.utc.sr03.services.UserService;
import jakarta.annotation.Nonnull;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import fr.utc.sr03.email.JakartaEmail;
import org.springframework.web.servlet.view.RedirectView;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;

/**
 * Web Controller for admin users.
 * In real life this controller should allow request only from a trusted origin. For testing purpose we assume that only only trusted user test our app.
 *
 * */

@Controller
@Validated
public class WebAdminController {
    @Resource
    private UserService userService;

    @Resource
    private ChannelService channelService;

    private static final Logger logger = Logger.getLogger(WebAdminController.class.getName());

    private boolean isAllowed(HttpSession session) {
        // Check if the user is not connected
        if (session.getAttribute("user") == null) {
            // Redirect to the login with an error
            return false;
        }

        User u = (User) session.getAttribute("user");
        if (!u.isAdmin()) {
            // redirect not allowed user
            return false;
        }
        return true;
    }

    @GetMapping(value = "/admin")
    public String admin(Model model,
                        @RequestParam(defaultValue = "0") int page, // num of the page. 0 by default
                        @RequestParam(defaultValue = "0") int pastPage, // num of the page for the pastUser section.
                        @RequestParam(defaultValue = "false") boolean activatePastUsersSection, // used to know if we need to switch auto to the past user section
                        @RequestParam(defaultValue = "") String query,
                        @RequestParam(defaultValue = "") String queryPast,
                        @Nonnull HttpSession session) {
        // sesion check       
        if (!isAllowed(session)){
            model.addAttribute("message", "Not Allowed");
            model.addAttribute("code", 405);
            return "error";
        }

        //ADMIN INFORMATION
        User u = (User) session.getAttribute("user");
        model.addAttribute("pageTitle", "The Chat, admin");
        model.addAttribute("title", "The Chat - Admin");
        model.addAttribute("user", u);


        //USERS INFORMATION
        List<User> users;
        if (Objects.equals(query, "")) {
            users = userService.getUsers() ; // All users that are in the database, TODO: write a userService method to fetch only  the ${size} users needed from the database
        } else {
            users = userService.searchUsers(query) ;
        }
        // pagination attribute for user list
        int size = 3 ;
        List<User> userPage = new ArrayList<>() ; // users that are displayed
        int index = page * size ;
        while (index < users.size() && index < (page+1) * size ) {
            userPage.add(users.get(index));
            index ++;
        }
        model.addAttribute("userPage", userPage);
        model.addAttribute("currentPage", page);
        model.addAttribute("totalPages", (users.size() % size == 0) ? (users.size() / size) : (users.size() / size) + 1);

        //PAST USERS INFORMATION
        List<User> pastUsers;
        if (Objects.equals(queryPast, "")) {
            pastUsers = userService.getPastUsers() ;
        } else {
            pastUsers = userService.searchPastUsers(queryPast) ;
        }
        List<User> pastUserPage = new ArrayList<>() ;
        int indexPast = pastPage * size ;
        while ( indexPast < pastUsers.size() && indexPast < (pastPage+1) * size ) {
            pastUserPage.add(pastUsers.get(indexPast));
            indexPast ++;
        }
        model.addAttribute("pastUserPage", pastUserPage);
        model.addAttribute("pastCurrentPage", pastPage);
        model.addAttribute("pastTotalUserPages", (pastUsers.size() % size == 0) ? (pastUsers.size() / size) : (pastUsers.size() / size) + 1);
        model.addAttribute("activatePastUsersSection", (activatePastUsersSection)?"true":"false");
        return "admin";
    }

    @PostMapping(value = "/deleteUser")
    public String deleteUser(Model model, @RequestParam(name = "id") int id, @Nonnull HttpSession session) {
        // sesion check
        if (!isAllowed(session)){
            model.addAttribute("message", "Not Allowed");
            model.addAttribute("code", 405);
            return "error" ;
        }

        User user = id > 0 ? userService.getUser(id) : null;
        if (user != null) {

            //Verify if there are no pending invitations => delete them
            List<Invitation> invitationsInviter = channelService.getInvitationsForUserInviter(user);
            if (!invitationsInviter.isEmpty())
            {
                for (Invitation invitation : invitationsInviter)
                    channelService.deleteInvitation(invitation.getId());
            }

            //Verify if there are no pending invitations => delete them
            List<Invitation> invitationsInvited = channelService.getInvitationsForUserInvited(user);
            if (!invitationsInvited.isEmpty())
            {
                for (Invitation invitation : invitationsInvited)
                    channelService.deleteInvitation(invitation.getId());
            }

            //Verify if there are no channels left behind => switch to another user
            List<Channel> channels = channelService.getChannels();
            for (Channel channel : channels)
                if (channel.isMember(user))
                {
                    channel.removeMember(user);
                    if (channel.isCreator(user)) {
                        channel.setCreator(channel.getRandomMember());
                    }
                    channelService.updateChannel(channel);
                }
            userService.deleteUser(user.getId());
        }
        logger.info("User "+ user.getLastName() + " - " + user.getFirstName()+" successfully deleted");
        return "redirect:/admin";
    }

    @PostMapping(value = "/createUser")
    public String createUser(Model model,
                        @Nonnull HttpSession session,
                        @RequestParam String firstName,
                        @RequestParam String lastName,
                        @RequestParam String email,
                        @RequestParam String password,
                        @RequestParam String admin) {
        // sesion check
        if (!isAllowed(session)){
            model.addAttribute("message", "Not Allowed");
            model.addAttribute("code", 405); // code used by thymeleaf to custom the message error on the login page.
            return "error" ;
        }

        boolean adminBool = admin.equals("admin");
        User user = new User(firstName, lastName, email, password, adminBool);
        try {
            userService.addUser(user);
        } catch (Exception e) {
            // pop up that email is already taken, stay on the same page.
            logger.warning("Attempt to create a user with an existing email Failed: " + email);
            return "redirect:admin#NewUser";
        }


        // Send mail to change the user password
        String from = "sesinian+SR03TEST@etu.utc.fr";
        String to = user.getEmail();
        String subject = "Welcome to The Chat ! Change your password now";
        String body = "<html><body><p><b>Welcome to the Chat !The Chat</b></p></br>" +
                "<b>Here is your password </b>: "+password+" </br>"+
                "You're now able to connect on : [inserer link react app]</br>"+
                "</body></html>";
        JakartaEmail.sendMail(from, to, subject, body);

        logger.info(" User " + user.getLastName() + " successfully created");
        return "redirect:/admin";
    }

    @PostMapping(value = "/toggleUser")
    public String toggleUser(Model model, @Nonnull HttpSession session, @RequestParam(name = "id") int id){
        // sesion check
        if (!isAllowed(session)){
            model.addAttribute("message", "Not Allowed");
            model.addAttribute("code", 405);
            return "error" ;
        }
        User userToToggle = id > 0 ? userService.getUser(id) : null;

        if (userToToggle.isActivated())
            userToToggle.setActivated(false);
        else
            userToToggle.setActivated(true);

        userService.updateUser(userToToggle);
        return "redirect:/admin";
    }

    @PostMapping(value = "modifyUser")
    public String modifyUser(HttpSession session ,Model model, @RequestParam(name = "id") int id) {
        if (!isAllowed(session)){
            model.addAttribute("message", "Not Allowed");
            model.addAttribute("code", 405);
            return "error" ;
        }
        model.addAttribute("pageTitle", "The Chat");

        User u = (User) session.getAttribute("user");
        User userToUpdate = id > 0 ? userService.getUser(id) : null;
        model.addAttribute("userToUpdate", userToUpdate);
        model.addAttribute("user", u);
        return "userForm";
    }

    @PostMapping(value="updateUser")
    public String updateUser(Model model,
                            @Nonnull HttpSession session,
                            @RequestParam(name = "id") int id,
                            @RequestParam String firstName,
                            @RequestParam String lastName,
                            @RequestParam String email,
                            @RequestParam String password,
                            @RequestParam String admin) {
        // sesion check
        if (!isAllowed(session)){
            model.addAttribute("message", "Not Allowed");
            model.addAttribute("code", 405);
            return "error" ;
        }

        boolean adminBool = admin.equals("admin");
        User userToUpdate = userService.getUser(id);

        // check if userToUpdate not null
        userToUpdate.setFirstName(firstName);
        userToUpdate.setLastName(lastName);
        userToUpdate.setEmail(email);
        if (!password.isEmpty()){
            userToUpdate.setPassword(password); // in case no password where provided, we preserve the previous password
        }
        userToUpdate.setAdmin(adminBool); // we don't change is activated status because there is already a button in the interface

        userService.updateUser(userToUpdate);
        return "redirect:/admin";
    }

    @PostMapping(value = "/logout")
    public String logout(@Nonnull HttpSession session) {
        session.invalidate();
        return "redirect:/login";
    }
}
