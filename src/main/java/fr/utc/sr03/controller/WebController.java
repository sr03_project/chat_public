package fr.utc.sr03.controller;


import at.favre.lib.crypto.bcrypt.BCrypt;
import fr.utc.sr03.model.Channel;
import fr.utc.sr03.model.Invitation;
import fr.utc.sr03.model.User;
import fr.utc.sr03.services.ChannelService;
import fr.utc.sr03.services.UserService;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.logging.Logger;

/**
 * Web Controller for admin users.
 * In real life this controller should allow request only from a trusted origin. For testing purpose we assume that only only trusted user test our app.
 *
 * */

@Controller
@SessionAttributes("user")
public class WebController {

    @Resource
    private UserService userService;

    @Resource
    private ChannelService channelService;

    private static final Logger logger = Logger.getLogger(WebController.class.getName());
    @RequestMapping(value = "/")
    public String index(Model model) {
        return "redirect:/login";
    }

    @GetMapping(value = "/login")
    public String login(Model model, @ModelAttribute("errorInfo") String errorInfo) {
        model.addAttribute("title", "The Chat - Admin log in");
        model.addAttribute("pageTitle", "The Chat");
        model.addAttribute("errorInfo", errorInfo);
        return "login";
    }

    @PostMapping(value = "/authenticate")
    public String authenticate(Model model,
                            HttpSession session,
                            RedirectAttributes redirectAttributes,
                            @RequestParam String email,
                            @RequestParam String password) {
        User u = userService.getUserByEmail(email);
        if (u == null) {
            redirectAttributes.addFlashAttribute("errorInfo", "User Not Found");
            return "redirect:/login";
        }

        // password check
        BCrypt.Result result = BCrypt.verifyer().verify(password.toCharArray(), u.getPassword());
        if ( result.verified ) {
            //passing the current user to the page (creating a specific one for the moment)
            session.setAttribute("user", u);
            if(u.isAdmin()){
                logger.info("New session for " + u.getFirstName() + " " + u.getLastName());
                return "redirect:/admin";
            }
            else{
                redirectAttributes.addFlashAttribute("errorInfo", "User interface not ready.");
                return "redirect:/login"; // now we don't handle user login.
            }
        }

        redirectAttributes.addFlashAttribute("errorInfo", "Wrong Password");
        return "redirect:/login";
    }

}