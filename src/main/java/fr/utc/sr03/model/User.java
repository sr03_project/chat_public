package fr.utc.sr03.model;


import at.favre.lib.crypto.bcrypt.BCrypt;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.annotation.Nonnull;
import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import org.springframework.lang.NonNull;

import javax.annotation.processing.Generated;
import java.util.*;
import java.util.regex.Pattern;

@Entity
@Table(name = "users")
public class User {

    //Attributes
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="user_id")
    private int id;

    @Column(name="firstname")
    @NotNull(message = "First name cannot be null")
    private String firstName;
    @Column(name="lastname")
    @NotNull(message = "Last name cannot be null")
    private String lastName;

    @Column(name="email")
    @Email(message = "Enter a valid mail")
    private String email;

    @JsonIgnore
    @Column(name="password") // as the hash of the password is stored, we can't apply annotation for validation here.
    private String password;

    @Column(name="admin")
    private boolean admin;

    @Column(name="activated")
    private boolean activated ;

    //Constructor
    public User(){}

    public User(String firstName, String lastName, String email, String password, boolean admin){
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.admin = admin;
        this.activated = true;
        // check password
        if (User.checkPassword(password)) {
            this.password = BCrypt.withDefaults().hashToString(12, password.toCharArray());
        } else {
            throw new Error("message = \"Password must contain at least one uppercase letter, one lowercase letter, one number, and one special character (@$!%*?&)\")\n") ;
        }
    }

    public static boolean checkPassword(String password) {
        String pattern = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$";
        return Pattern.matches(pattern, password);
    }

    //Getters & Setters

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = BCrypt.withDefaults().hashToString(12, password.toCharArray());
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public int getId() {
        return id;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    //Equals & Hash
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User user)) return false;
        return id == user.id && Objects.equals(email.toLowerCase(), user.email.toLowerCase());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, email);
    }


    //Methods
}
