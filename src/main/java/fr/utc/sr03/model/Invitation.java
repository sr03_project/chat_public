package fr.utc.sr03.model;

import jakarta.persistence.*;

@Entity
@Table(name = "invitations")
public class Invitation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="invitation_id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "inviter_id", referencedColumnName = "user_id", nullable = false)
    private User inviter;

    @ManyToOne
    @JoinColumn(name = "invited_id", referencedColumnName = "user_id", nullable = false)
    private User invited;

    @ManyToOne
    @JoinColumn(name = "channel_id", referencedColumnName = "channel_id", nullable = false)
    private Channel channel;



    // Constructors, getters, setters


    public Invitation(User inviter, User invited, Channel channel) {
        this.inviter = inviter;
        this.invited = invited;
        this.channel = channel;
    }

    public Invitation() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getInviter() {
        return inviter;
    }

    public void setInviter(User inviter) {
        this.inviter = inviter;
    }

    public User getInvited() {
        return invited;
    }

    public void setInvited(User invited) {
        this.invited = invited;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }


    //Equals & Hash
    @Override
    public boolean equals(Object o) {
        if(this == o) return true;
        if(!(o instanceof Invitation invitation)) return false;
        // Invitations are the same only if they both have same id, or the user involved and the channel are the same.
        return this.id == invitation.getId() || ( this.invited == invitation.getInvited() && this.channel == invitation.getChannel() );
    }
    //Methods
}