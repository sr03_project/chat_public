package fr.utc.sr03.websocket;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.utc.sr03.model.Channel;
import fr.utc.sr03.model.User;
import fr.utc.sr03.services.ChannelService;
import fr.utc.sr03.services.UserService;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;


import java.io.IOException;
import java.util.*;


public class WebSocketHandler extends TextWebSocketHandler {

    private final UserService userService;
    private final ChannelService channelService;

    private final Logger logger = Logger.getLogger(WebSocketHandler.class.getName());
    private final Map<Integer, List<WebSocketSession>> activeSessionsByChatId = new HashMap<>();
    private final Map<Integer,List<User>> activeUtilisateursByChatId = new HashMap<>();

    public WebSocketHandler(ChannelService channelService, UserService userService) {
        this.channelService = channelService;
        this.userService = userService;
    }

    @Override
    public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws IOException {

        ObjectMapper mapper = new ObjectMapper();

        Integer channelId = Integer.parseInt((String) session.getAttributes().get("channelId"));
        Integer userId = Integer.parseInt((String) session.getAttributes().get("userId"));
        String msgToSend = (String) message.getPayload();
        List<WebSocketSession> sessionsThatWillReceiveTheMessage = activeSessionsByChatId.get(channelId);
        synchronized (activeSessionsByChatId) {
            for (WebSocketSession s : sessionsThatWillReceiveTheMessage) {
                if (s.isOpen()) {
                    try {
                        s.sendMessage(new TextMessage(msgToSend));
                    } catch (IOException e) {
                        logger.error("cannot send message to " + s.getRemoteAddress() + "  " +e);
                    }
                }
            }
        }
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws IOException {
        try {
            String uri = String.valueOf(session.getUri());
            String[] pathSegments = uri.split("/");
            String channelId = pathSegments[pathSegments.length - 2];
            String userId = pathSegments[pathSegments.length - 1];
            session.getAttributes().put("channelId", channelId);
            session.getAttributes().put("userId", userId);
            addSession(Integer.parseInt(channelId), session);
            
            //System.out.println(channelId);
            User user = userService.getUser(Integer.parseInt(userId));
            addConnectedMember(Integer.parseInt(channelId), user);
            Channel channel = channelService.getChannel(Integer.parseInt(channelId));

            //Envoie du message de bienvenue au nouvel arrivant
            MessageSocket messageDeBienvenue = new MessageSocket();
            messageDeBienvenue.setUser("Admin ");
            messageDeBienvenue.setMessage("Bienvenue à " +user.getFirstName()+" sur "+ channel.getTitle());
            //System.out.println(msg.toString());
            session.sendMessage(new TextMessage(messageDeBienvenue.toString()));
            logger.info("Connecté sur le " + channel.getTitle());

            //Envoie de la mise à jour de connexion aux utilisateurs
            MessageSocket msg = new MessageSocket();
            msg.setConnectedUsers(activeUtilisateursByChatId.get(Integer.parseInt(channelId)));
            List<WebSocketSession> sessionsThatWillReceiveTheMessage = activeSessionsByChatId.get(Integer.parseInt(channelId));
            synchronized (activeSessionsByChatId) {
                for (WebSocketSession s : sessionsThatWillReceiveTheMessage) {
                    if (s.isOpen()) {
                        try {
                            s.sendMessage(new TextMessage(msg.toString()));
                        } catch (IOException e) {
                            logger.error("cannot send message to " + s.getRemoteAddress() + "  " +e);
                        }
                    }
                }
            }
        } catch (IOException e) {
            logger.error("Failed to establish connection with session " + session + "  " +e);
        }
    }
    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) {
        Integer channelId = Integer.parseInt((String) session.getAttributes().get("channelId"));
        Integer userId = Integer.parseInt((String) session.getAttributes().get("userId"));
        User user = userService.getUser(userId);
        activeUtilisateursByChatId.get(channelId).remove(user);
        Channel channel = channelService.getChannel(channelId);
        logger.info("Deconnected from chat "+channel.getTitle()+ " : "+user.getFirstName()+" "+user.getLastName());
        activeSessionsByChatId.get(channelId).remove(session);

        // Notify the update to other sessions
        MessageSocket msg = new MessageSocket();
        msg.setConnectedUsers(activeUtilisateursByChatId.get(channelId));
        List<WebSocketSession> sessionsThatWillReceiveTheMessage = activeSessionsByChatId.get(channelId);
        synchronized (activeSessionsByChatId) {
            for (WebSocketSession s : sessionsThatWillReceiveTheMessage) {
                if (s.isOpen()) {
                    try {
                        s.sendMessage(new TextMessage(msg.toString()));
                    } catch (IOException e) {
                        logger.error("cannot send message to " + s.getRemoteAddress() + "  " +e);
                    }
                }
            }
        }
    }


    private void addSession(Integer chatId, WebSocketSession session) {
        System.out.println(chatId);
        activeSessionsByChatId.computeIfAbsent(chatId, k -> new ArrayList<>());
        activeSessionsByChatId.get(chatId).add(session);
    }

    private void addConnectedMember(Integer chatId, User user) {
        activeUtilisateursByChatId.computeIfAbsent(chatId, k -> new ArrayList<>());
        if (!activeUtilisateursByChatId.get(chatId).contains(user)) {
            activeUtilisateursByChatId.get(chatId).add(user);
        }
    }
}

