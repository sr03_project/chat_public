package fr.utc.sr03.websocket;

import com.google.gson.Gson;
import fr.utc.sr03.model.User;

import java.util.Date;
import java.util.List;

public class MessageSocket {

    private String user;
    private String message;
    private Date timestamp;
    private List<User> connectedMembers;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public List<User> getConnectedUsers() {
        return connectedMembers;
    }

    public void setConnectedUsers(List<User> connectedMembers) {
        this.connectedMembers = connectedMembers;
    }

    @Override
    public String toString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
