package fr.utc.sr03.websocket;


import fr.utc.sr03.model.Channel;
import fr.utc.sr03.services.ChannelService;
import fr.utc.sr03.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

import java.util.List;


@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

    @Autowired
    private UserService userService;

    @Autowired
    private ChannelService channelService;

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(new WebSocketHandler(this.channelService, this.userService), "/salons/{channelId}/{userId}").setAllowedOrigins("*");
    }
}
